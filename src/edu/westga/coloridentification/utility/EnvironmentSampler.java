/**
 * 
 */
package edu.westga.coloridentification.utility;

import edu.westga.coloridentification.model.Color;
import edu.westga.coloridentification.model.ColorSensor;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.robotics.SampleProvider;

/**
 * Environment perceptor for an EV3.
 *
 * @author Kathyrn Browning
 * @author Destiny Harris
 *
 * @date November 14, 2016
 * @version CS3270 Intelligent Systems
 *
 */
public class EnvironmentSampler {

	public static final int ADJUSTMENT = 950;

	/**
	 * Gets a reading for the specified sensor.
	 *
	 * @precondition : sensor must not be null.
	 *
	 * @param sensor
	 *            : the sensor to read a sample from.
	 * @return the sample read from the sensor.
	 */
	public static Color getSampleFrom(SampleProvider sensor) {
		if (sensor == null) {
			throw new IllegalArgumentException("Sensor must not be null when trying to read.");
		}

		float[] sample = new float[sensor.sampleSize()];
		sensor.fetchSample(sample, 0);
		return new Color(sample[0], sample[1], sample[2]);
	}

	/**
	 * Runs a testing scenario and displays results.
	 * 
	 * @precondition: none
	 * @postcondition: a testing scenario is run and the results are displayed
	 *                 on the EV3
	 * 
	 * @param sensor:
	 *            the sensor to read a sample from.
	 */
	public static void runTestDisplay(ColorSensor sensor) {

		sensor.setCurrentMode("RGB");
		while (Button.ENTER.isUp()) {
			LCD.drawString("Testiong RGB Mode", 0, 0);
			Color color = sensor.scan();
			LCD.drawString(color.getRed() + ", " + color.getGreen() + ", " + color.getBlue(), 0, 4);

			try {
				Thread.sleep(1000);
				LCD.refresh();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
