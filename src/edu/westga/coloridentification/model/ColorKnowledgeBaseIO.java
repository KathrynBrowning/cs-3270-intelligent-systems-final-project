/**
 * 
 */
package edu.westga.coloridentification.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import edu.westga.coloridentification.model.Color;

/**
 * @author Kathryn Browning
 * @author Destiny Harris
 * 
 * @date November 16, 2016
 * @version CS3270 Intelligent Systems
 *
 */
public class ColorKnowledgeBaseIO {

	/**
	 * Reads the data in the specified file path.
	 * 
	 * @param filepath
	 *            the location of the file to read.
	 * 
	 * @return A set of colors and their names from the file.
	 */
	public static Map<String, Color> Read(String filepath) {

		Map<String, Color> colors = new HashMap<String, Color>();

		try {
			File colorData = new File(filepath);
			Scanner scanner = new Scanner(colorData);
			while (scanner.hasNext()) {
				String[] colorRecord = scanner.nextLine().split(",");
				String[] colorRGB = colorRecord[0].split("-");

				int red = Integer.parseInt(colorRGB[0]);
				int green = Integer.parseInt(colorRGB[1]);
				int blue = Integer.parseInt(colorRGB[2]);

				String colorName = colorRecord[1];
				Color color = new Color((float) (red / 255), (float) (green / 255), (float) (blue / 255));

				colors.put(colorName, color);
			}

			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return colors;
	}

	/**
	 * Update dates the file with all colors and color names.
	 * 
	 * @param colors
	 *            the colors to update to the file.
	 * @param filepath
	 *            the location of the file to update.
	 */
	public static void Update(Map<String, Color> colors, String filepath) {

		Reset(filepath);

		for (String colorName : colors.keySet()) {
			Update(colors.get(colorName), colorName, filepath);
		}
	}

	/**
	 * Establishes a fresh file to store data to.
	 * 
	 * @param filepath
	 *            the location of file to make.
	 */
	public static void Reset(String filepath) {
		try {

			File colorData = new File(filepath);

			if (colorData.exists()) {
				colorData.delete();
			}

			colorData.createNewFile();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Updates a given file with a specified color and its color name.
	 * 
	 * @param color
	 *            the color to update with.
	 * @param name
	 *            the name of the color to update with.
	 * @param filepath
	 *            the location of file to update.
	 */
	public static void Update(Color color, String name, String filepath) {
		try {

			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filepath), true));

			String line = color.getRed() + "-" + color.getGreen() + "-" + color.getBlue() + "," + name;
			writer.write(line);
			writer.append("\r\n");
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Determine's the closets color relative from the specified file to the
	 * sample color provided.
	 * 
	 * @param sampleColor
	 *            the color to compare with.
	 * @param filepath
	 *            the location of the file.
	 * @return the closest color relative to the provided sample color.
	 */
	public static Color findClosestColorTo(Color sampleColor, String filepath) {

		Map<String, Color> colorData = Read(filepath);

		float[] hsb = convertColorToHSB(sampleColor);
		float hue = hsb[0];
		float saturation = hsb[1];
		float brightness = hsb[2];

		if (saturation < 0.1 && brightness > 0.9) {
			return colorData.get("White Tone");
		} else if (brightness < 0.1) {
			return colorData.get("Black Tone");
		} else {

			float deg = hue * 360;

			if (deg >= 0 && deg < 30) {
				return colorData.get("Red Tone");
			} else if (deg >= 30 && deg < 90) {
				return colorData.get("Yellow Tone");
			} else if (deg >= 90 && deg < 150) {
				int diff = Math.abs(sampleColor.getGreen() - sampleColor.getBlue());
				if (diff <= 15) {
					return colorData.get("Blue Tone");
				} else {
					return colorData.get("Green Tone");
				}

			} else if (deg >= 150 && deg < 210) {
				return colorData.get("Cyan Tone");
			} else if (deg >= 210 && deg < 270) {
				return colorData.get("Blue Tone");
			} else if (deg >= 270 && deg < 330) {
				return colorData.get("Magenta Tone");
			} else {
				return colorData.get("Red Tone");
			}
		}
	}

	private static float[] convertColorToHSB(Color sampleColor) {
		float hsb[] = new float[3];
		int r = (sampleColor.getRGB() >> 16) & 0xFF;
		int g = (sampleColor.getRGB() >> 8) & 0xFF;
		int b = (sampleColor.getRGB()) & 0xFF;
		Color.RGBtoHSB(r, g, b, hsb);
		return hsb;
	}

	/**
	 * Gets the name of a specified color from a specified file.
	 * 
	 * @param color
	 *            the color who's name should be retrieved.
	 * @param filepath
	 *            the location of the file where the color is stored.
	 * @return the name of the specified color from the file.
	 */
	@SuppressWarnings("rawtypes")
	public static String getName(Color color, String filepath) {
		Color temp = findClosestColorTo(color, filepath);

		if (temp != null) {
			Map<String, Color> colorData = Read(filepath);
			for (Map.Entry entry : colorData.entrySet()) {
				if (temp.equals(entry.getValue())) {
					return (String) entry.getKey();
				}
			}

			return "Can Not Find";
		}

		return "Can Not Find";

	}

	public static Color getColor(String name, String filepath) {
		Map<String, Color> colorData = Read(filepath);
		return colorData.get(name);
	}
}
