/**
 * 
 */
package edu.westga.coloridentification.model;

import edu.westga.coloridentification.utility.EnvironmentSampler;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import edu.westga.coloridentification.model.Color;

/**
 * A calibrated light sensor for an EV3.
 *
 * @author Kathyrn Browning
 * @author Destiny Harris
 *
 * @date November 14, 2016
 * @version CS3270 Intelligent Systems
 *
 */
public class ColorSensor extends EV3ColorSensor {

	/**
	 * Creates a light sensor for an EV3.
	 *
	 * @precondition : port must not be null.
	 * @postcondition : getLightThreshold() == 0;
	 *
	 * @param port
	 *            : The port to connection point between the sensor and EV3
	 *            brick.
	 */
	public ColorSensor(Port port) {
		super(port);
	}

	/**
	 * Reads the current light ambiance of its environment.
	 *
	 * @precondition : None
	 * @postcondition: An RGB sample is gotten from the sensor
	 *
	 * @return the current light ambiance of its environment.
	 */
	public Color scan() {
		Color color = EnvironmentSampler.getSampleFrom(this.getRGBMode());
		return color;
	}

}
