package edu.westga.coloridentification.model;

import java.util.HashMap;
import java.util.Map;

import lejos.hardware.Button;

/**
 * This class runs trains the EV3 on different colors and updates its internal
 * file to include the new colors.
 * 
 * @author Kathryn Browning
 * @author Destiny Harris
 * 
 * @date November 21, 2016
 * @version CS3270 Intelligent Systems
 *
 */
public class Rover {

	private ColorSensor sensor;
	private final static String BRAIN_FILE = "test.csv";
	private Map<String, Color> knownColors;

	/**
	 * Trains the EV3 and updates its color knowledge base.
	 * 
	 * @precondition: None
	 * @postcondition: None
	 * 
	 * @param sensor
	 *            : the sensor to read a sample from.
	 */
	public Rover(ColorSensor sensor) {
		this();
		this.sensor = sensor;
	}

	/**
	 * Trains the EV3 and updates its color knowledge base.
	 * 
	 * @precondition: None
	 * @postcondition: The sensor is null
	 * 
	 */
	public Rover() {
		this.sensor = null;
		this.knownColors = new HashMap<String, Color>();
	}

	/**
	 * Trains the EV3 and displays the training results along with the correct
	 * color from the color knowledge base.
	 * 
	 * @precondition: None
	 * @postcondition: The closest color to the scanned color is displayed.
	 */
	public void run() {
		this.train();

		while (Button.ESCAPE.isUp()) {
			Color chosenColor = this.displayColorReadings();
			System.out.println(ColorKnowledgeBaseIO.getName(chosenColor, BRAIN_FILE));
			Button.RIGHT.waitForPressAndRelease();
		}
	}

	private void train() {
		ColorKnowledgeBaseIO.Reset(BRAIN_FILE);
		ColorKnowledgeBaseIO.Update(new Color(1, 0, 0), "Red Tone", BRAIN_FILE);
		ColorKnowledgeBaseIO.Update(new Color(0, 1, 0), "Green Tone", BRAIN_FILE);
		ColorKnowledgeBaseIO.Update(new Color(0, 0, 1), "Blue Tone", BRAIN_FILE);
		ColorKnowledgeBaseIO.Update(new Color(1, 1, 0), "Yellow Tone", BRAIN_FILE);
		ColorKnowledgeBaseIO.Update(new Color(0, 1, 1), "Cyan Tone", BRAIN_FILE);
		ColorKnowledgeBaseIO.Update(new Color(1, 0, 1), "Magenta Tone", BRAIN_FILE);
		ColorKnowledgeBaseIO.Update(new Color(1, 1, 1), "White Tone", BRAIN_FILE);
		ColorKnowledgeBaseIO.Update(new Color(0, 0, 0), "Black Tone", BRAIN_FILE);

		this.knownColors = ColorKnowledgeBaseIO.Read(BRAIN_FILE);

		for (String name : this.knownColors.keySet()) {
			System.out.println(name);
			System.out.println(((Color) (this.knownColors.get(name))).toString());
		}
	}

	private Color displayColorReadings() {
		Color color = this.sensor.scan();

		while (Button.ENTER.isUp()) {
			System.out.println(color.toString());
			color = this.sensor.scan();
		}

		return color;
	}
}
