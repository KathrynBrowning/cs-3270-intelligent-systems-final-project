/**
 * 
 */
package edu.westga.coloridentification.model;

/**
 * Represents a color with certain attributes.
 * 
 * @author Kathryn Browning
 * @author Destiny Harris
 * 
 * @date November 16, 2016
 * @version CS3270 Intelligent Systems
 */
@SuppressWarnings("serial")
public class Color extends java.awt.Color {

	/**
	 * Creates a Color object based on a red percentage value, green percentage
	 * value, and blue percentage value.
	 * 
	 * @param red
	 *            the percentage of red present in a Color.
	 * @param green
	 *            the percentage of green present in a Color.
	 * @param blue
	 *            the percentage of blue present in a Color.
	 */
	public Color(float red, float green, float blue) {
		super(red, green, blue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.Color#toString()
	 * 
	 * Displays the RGB values of a Color.
	 */
	@Override
	public String toString() {
		return "RGB( " + getRed() + "," + getGreen() + "," + getBlue() + " )";
	}

}
