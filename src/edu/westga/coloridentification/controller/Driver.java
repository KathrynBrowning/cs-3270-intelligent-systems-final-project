/**
 * 
 */
package edu.westga.coloridentification.controller;


import edu.westga.coloridentification.model.ColorSensor;
import edu.westga.coloridentification.model.Rover;
import lejos.hardware.port.SensorPort;


/**
 * The entry point of program.
 * 
 * @author Kathryn Browning
 * @author Destiny Harris
 * 
 * @date November 16, 2016
 * @version CS3270 Intelligent Systems
 */
public class Driver {

	/**
	 * @param args arguments for the program (not used).
	 */
	public static void main(String[] args) {
		Rover june = new Rover(new ColorSensor(SensorPort.S1));
		june.run();
	}

}
